<?php
/*
 * You are given an array of integers, @ints.
 * Write a script to find the third distinct maximum in the given array. 
 * If third maximum doesn’t exist then return the maximum number.
 */

echo "Enter a list of integers: ";
$response = fgets(STDIN);

$ints = explode(" ", chop($response));

echo "The third maximum is: " . find_third_maximum($ints) . PHP_EOL; 

function find_third_maximum($list): int
{
	sort($list, SORT_NUMERIC); // sort from smallest to largest
	$list = array_reverse($list);
	$list = array_unique($list);
	$list = array_values($list); // normalize array keys

	if (count($list) >= 3) {
		return $list[2];
	} else {
		return $list[0];
	}
}
