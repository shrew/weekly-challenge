# You are given a string, $str, and a 26-item array @widths containing the width of each character from a to z.
# Write a script to find out the number of lines and the width of the last line needed to display the given string,
# assuming you can only fit 100 width units on a line.

# the written explanation for this one is a little confusing so heres a link to the examples:
# https://theweeklychallenge.org/blog/perl-weekly-challenge-267/#TASK2

proc findNum {letter} {
	set letter [string toupper $letter]
	return [expr {[scan $letter %c] - 65}]
}

proc toLines {str widths} {
	set str [split $str ""]
	
	set width 0
	set lineCount 1
	
	foreach char $str {
		set charw [lindex $widths [findNum $char]]
		
		if {$width + $charw > 100} {
			incr lineCount
			incr width -100
			if {$width < $charw} {
				set width $charw
			}
		} else {
			incr width $charw
		}
	}
	return [list $lineCount $width]
}

# example 1
set str "abcdefghijklmnopqrstuvwxyz"
set widths {10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10}

puts [toLines $str $widths]

# example 2
set str "bbbcccdddaaa"
set widths {4 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10}

puts [toLines $str $widths]
