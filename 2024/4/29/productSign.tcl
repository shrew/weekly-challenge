# You are given an array of @ints.
# Write a script to find the sign of product of all integers in the given array.
# The sign is 1 if the product is positive, -1 if the product is negative and 0 if product is zero

proc sign {ints} {
	set product 1
	foreach i $ints {
		set product [expr {$product * $i}]
		
		if {![string is integer $i]} {
			puts stderr "\"$i\" is not an integer, but i'll allow it..."
		}
	}
	
	if {$product > 0} {
		return 1
	} elseif {$product < 0} {
		return -1
	} else {
		return 0
	}
}

puts -nonewline "Enter a list of integers: "
flush stdout

try {
	puts [sign [gets stdin]]
} on error {} {
	puts stderr "you cant multiply a non-number, silly"
}
