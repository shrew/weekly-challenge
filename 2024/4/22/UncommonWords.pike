/*
	You are given two sentences, $line1 and $line2. 
	Write a script to find all uncommmon words in any order in the given two sentences. 
	Return ('') if none found.
*/

int main(int argc, array(string) argv) {
	write("Enter first line: ");
	string line1 = Stdio.stdin.gets();
	
	write("Enter second line: ");
	string line2 = Stdio.stdin.gets();
	
	write("The mismatched words are:\n");
	foreach (findUncommon(line1, line2), string word) write("%s\n", word);
		
	return 0;
}

array(string) findUncommon(string line1, string line2) {
	array(string) checked = ({});
	array(string) found = ({});
	
	foreach (line1/" ", string word) {
		if (!has_value(checked, word)) {
			if (!has_value(line2, word)) found = Array.push(found, word);
			checked = Array.push(checked, word);
		}
	}
	
	foreach (line2/" ", string word) {
		if (!has_value(checked, word)) {
			if (!has_value(line1, word)) found = Array.push(found, word);
			checked = Array.push(checked, word);
		}
	}
	
	return found;
}