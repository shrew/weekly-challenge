<?php

// You are given a m x n binary matrix.
// Write a script to return the row number containing maximum ones, 
// in case of more than one rows then return smallest row number.

$matrix = [ [false, false, true],
			[true, false, true],
		  ];

echo "The truthiest row is: " . most_true_row($matrix) . "\n";

function most_true_row(array $matrix): int {
	$largest_value = ["row" => null, "sum" => null];
	
	foreach ($matrix as $row => $values) {
		$sum = array_sum($values);
		if ($sum > $largest_value["sum"]) {
			$largest_value = ["row" => $row, "sum" => $sum];
		}
	}
	
	return $largest_value["row"];
}